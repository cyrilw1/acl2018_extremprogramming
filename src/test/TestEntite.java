package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Case;
import model.Entite;
import model.Labyrinthe;
import model.PacmanGame;
import model.Personnage;

class TestEntite {

    @Test
    public void testDeplacer() {
        Personnage p = new Personnage(10, 10, null);
        p.deplacer(30, 40);
        assertEquals(30, p.getPosX(), "Position en x devrait etre :");
        assertEquals(40, p.getPosY(), "Position en y devrait etre :");
    }
	
/*	@Test
	public void testPeutDeplacer() {
		String s = "src/test/labTest.txt";
		PacmanGame game = new PacmanGame(s);
		assertEquals(false, game.peutDeplacer(0, 0), "On ne devrait pas pouvoir se deplacer ");
		assertEquals(true, game.peutDeplacer(20, 20), "On devrait pouvoir se deplacer ");
	}*/

    @Test
    public void test_SubirDegat_CasNormal() {
        Entite e = new Personnage(10, 10, null);
        assertEquals(200, e.getVie(), "Le personnage devrait avoir 200 pv");
        e.subirDegat(10);
        assertEquals(190, e.getVie(), "Le personnage devrait avoir 190 pv");
    }

    @Test
    public void test_SubirDegat_CasDegatNegatif() {
        Entite e = new Personnage(10, 10, null);
        assertEquals(200, e.getVie(), "Le personnage devrait avoir 200 pv");
        e.subirDegat(-10);
        assertEquals(200, e.getVie(), "Le personnage devrait avoir 200 pv");
    }

    @Test
    public void test_SubirDegat_CasDegatEntraineMortEntite() {
        Entite e = new Personnage(10, 10, null);
        e.subirDegat(200);
        assertEquals(0, e.getVie(), "Le personnage devrait avoir 0 pv");
        assertEquals(true, e.etreMort(), "Le personnage devrait etre mort");
    }

}
