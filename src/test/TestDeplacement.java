package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Labyrinthe;
import model.Personnage;

class TestDeplacement {

    private Labyrinthe l;
    private Personnage p;


    public void setUp() {
        l = new Labyrinthe(4, 4, 1);
        l.creerLabyFromTxt("src/test/labTest.txt");
        p = new Personnage(10, 10, l);
    }


    @Test
    public void test() {
        l.dessinerLabyrintheConsole();
        p.deplacer(3, 3);
        assertEquals( "Position en x devrait etre :", 3, p.getPosX());
        assertEquals("Position en y devrait etre :",3, p.getPosY());
    }

    @Test
    public void testPeutDeplacer() {
        assertEquals("On ne devrait pas pouvoir se deplacer ", false, p.peutDeplacer('c', 0, 0));
        assertEquals("On devrait pouvoir se deplacer ", true, p.peutDeplacer('c', 20, 20));
    }

}
