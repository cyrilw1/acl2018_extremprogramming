package test;



import static org.junit.Assert.assertEquals;

import org.junit.Test;

import model.Case;
import model.CaseMur;
import model.Labyrinthe;

public class LabyrintheTest {

    @Test
    public void testCreerLabyFromText() {
        Labyrinthe l = new Labyrinthe(4, 4, 1);
        l.creerLabyFromTxt("src/test/labTest.txt");
        Case[][] tab = l.getLab();
        l.dessinerLabyrintheConsole();
        System.out.println(tab[1][2].getType());
        assertEquals("Mur", tab[0][0].getType(), "La case est incorrecte");
        assertEquals("Mur", tab[0][1].getType(), "La case est incorrecte");
        assertEquals("Mur", tab[0][2].getType(), "La case est incorrecte");
        assertEquals("Mur", tab[0][3].getType(), "La case est incorrecte");

        assertEquals("Mur", tab[1][0].getType(), "La case est incorrecte");
        assertEquals("Vide", tab[1][1].getType(), "La case est incorrecte");
        assertEquals("Vide", tab[1][2].getType(), "La case est incorrecte");
        assertEquals("Mur", tab[1][3].getType(), "La case est incorrecte");

        assertEquals("Mur", tab[2][0].getType(), "La case est incorrecte");
        assertEquals("Mur", tab[2][1].getType(), "La case est incorrecte");
        assertEquals("Vide", tab[2][2].getType(), "La case est incorrecte");
        assertEquals("Mur", tab[2][3].getType(), "La case est incorrecte");

    }


    @Test
    public void testAjouterCase() {
        Labyrinthe l = new Labyrinthe(2, 2, 1);
        l.ajouterCase(0, 0, new CaseMur(0, 0));
        Case[][] tab = l.getLab();
        assertEquals("Mur", tab[0][0].getType(), "La case 0,0 est incorrecte");
    }


    @Test
    public void test_Generer_1_Monstre() {
        Labyrinthe l = new Labyrinthe(4, 4);
        l.creerLabyFromTxt("src/test/labTest.txt");
        l.genererMonstres(0);
        assertEquals("Il ne devrait y avoir qu'1 seul monstre", 1, l.getMonstres().size());
    }


    @Test
    public void test_Generer_0_Monstre() {
        Labyrinthe l = new Labyrinthe(4, 4);
        l.creerLabyFromTxt("src/test/labTest.txt");
        l.genererMonstres(0);
        assertEquals("Il ne devrait y avoir qu'1 seul monstre",1, l.getMonstres().size());
    }


}
