package test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Fantome;
import model.Labyrinthe;
import model.Monstre;
import model.Personnage;

class TestMonstre {
	
	private Labyrinthe l;
	
	@BeforeEach
	void initDataGame() {
		//Creation du jeu de donn�es
		l = new Labyrinthe(4, 4);
	}

	@Test
	void test_AttaquerCasEnnemiCaC() {
		Personnage p = new Personnage(0,0, l);
		Monstre m = new Fantome(0,1, l);
		l.setPerso(p);
		assertEquals(false, m.getEtatAttaque(), "etatAttaque devrait etre a faux");
		m.attaquer();
		assertEquals(true, m.getEtatAttaque(), "etatAttaque devrait etre a vrai");
	}
	
	@Test 
	void test_AttaquerCasEnnemiInatteignable() {
		Personnage p = new Personnage(0,0, l);
		Monstre m = new Fantome(0,3, l);
		l.setPerso(p);
		assertEquals(false, m.getEtatAttaque(), "etatAttaque devrait etre a faux");
		m.attaquer();
		assertEquals(false, m.getEtatAttaque(), "etatAttaque devrait rester a faux");
	}
}
