package test;

import model.Astar;
import model.Edge;
import model.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AstarTest {
    private final int LABYLENGTH = 40;
    private Astar astar;
    private Node n0;
    private Node n1;
    private Node n2;
    private Node n3;
    private Node n4;
    private Node n5;
    private Node n6;
    private Node n7;
    private Node n8;
    private Node n9;
    private Node n10;
    private Node n11;

    @BeforeEach
    private void setUp() {
        astar = new Astar();
        n0 = new Node(0, 0, 0);
        n1 = new Node(1, 1, 0);
        n2 = new Node(2, 2, 0);
        n3 = new Node(3, 3, 0);
        n4 = new Node(4, 0, 1);
        n5 = new Node(5, 1, 1);
        n6 = new Node(6, 2, 1);
        n7 = new Node(7, 3, 1);
        n8 = new Node(8, 0, 2);
        n9 = new Node(9, 1, 2);
        n10 = new Node(10, 2, 2);
        n11 = new Node(11, 3, 2);

        n0.addVoisin(new Edge(n1, 5));
        n0.addVoisin(new Edge(n3, 8));

        n1.addVoisin(new Edge(n0, 10));
        n1.addVoisin(new Edge(n4, 2));

        n2.addVoisin(new Edge(n5, 8));
        n2.addVoisin(new Edge(n6, 6));

        n3.addVoisin(new Edge(n7, 12));
        n3.addVoisin(new Edge(n9, 2));

        n4.addVoisin(new Edge(n10, 2));

        n5.addVoisin(new Edge(n3, 2));

        n6.addVoisin(new Edge(n2, 2));
        n6.addVoisin(new Edge(n8, 2));

        n7.addVoisin(new Edge(n8, 4));

        n8.addVoisin(new Edge(n7, 6));

        n9.addVoisin(new Edge(n0, 2));

        n10.addVoisin(new Edge(n1, 4));
        n10.addVoisin(new Edge(n6, 6));

        n11.addVoisin(new Edge(n4, 8));
        n11.addVoisin(new Edge(n9, 3));
    }

    @Test
    private void startEqualEnd() {
        Node start = new Node(LABYLENGTH * 3 + 2, 2, 3);
        Node end = new Node(LABYLENGTH * 3 + 2, 2, 3);
        astar.AstarSearch(start, end);
        ArrayList<Node> exepectedPath = new ArrayList<Node>();
        assertEquals(astar.printPath(end), exepectedPath, "Le chemin trouvé ne correspond pas");
    }

    @Test
    private void printPathTest() {
        Node start = new Node(LABYLENGTH * 3 + 2, 2, 3);
        Node end = new Node(LABYLENGTH * 8 + 8, 8, 8);
        astar.AstarSearch(start, end);
        ArrayList<Node> exepectedPath = new ArrayList<Node>();
        exepectedPath.add(n1);
        exepectedPath.add(n4);
        exepectedPath.add(n10);
        exepectedPath.add(n6);
        exepectedPath.add(n8);
        exepectedPath.add(n11);

        assertEquals(exepectedPath, astar.printPath(end), "Le chemin retourné est faux");
    }
}