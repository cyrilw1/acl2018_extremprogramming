package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import engine.GamePainter;

/**
 * @author Horatiu Cirstea, Vincent Thomas
 *
 * afficheur graphique pour le game
 * 
 */
public class PacmanPainter implements GamePainter {

	/**
	 * jeu a afficher qui contient les éléments (perosnnage, labyrinthe, monstre)
	 * 
	 */
	private PacmanGame game;
	
	
	/**
	 * la taille des cases
	 */
	public static final int DIM = 1000;
	public static final int TAILLE_CASE = 25;
	public static final int RAPPORT = DIM/TAILLE_CASE;

	/**
	 * appelle constructeur parent
	 * 
	 * @param game
	 *            le jeutest a afficher
	 */
	public PacmanPainter(PacmanGame g) {
		this.game = g;
	}

	/**
	 * methode  redefinie de Afficheur retourne une image du jeu
	 */
	@Override
	public void draw(BufferedImage im) {
		Graphics2D crayon = (Graphics2D) im.getGraphics();
		game.dessiner(crayon);
	}

}
