package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Zombie extends Monstre {
	
	private BufferedImage zombieImg = null;
    private TexturePaint zombieSprite;
	private static final int TAILLE_CASE = PacmanPainter.TAILLE_CASE;


	public Zombie(int x, int y, Labyrinthe lab) {
		super(x, y, lab);
		pvMax = 5;
		pv = pvMax;
		degat = 1;
		zombieImg = Texture.getInstance().ZOMBIE;

		zombieSprite = new TexturePaint(zombieImg, new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
	}

	@Override
	/**
	 * Methode de deplacament al�atoire du zombie
	 */
	public void deplacer() {
		double d = Math.random();
        char vecteur;
        if (d < 0.25) {
        	vecteur = 'h';
            if (peutDeplacer(vecteur, posX, posY-1)) //monter
                this.haut();
        } else if (d < 0.5) {
        	vecteur = 'b';
            if (peutDeplacer(vecteur, posX, posY+1)) //descendre
                this.bas();
        } else if (d < 0.75) {
        	vecteur = 'g';
            if (peutDeplacer(vecteur, posX-1, posY)) //gauche
                this.gauche();
        } else {
        	vecteur = 'd';
            if (peutDeplacer(vecteur, posX+1, posY))   //droite
                this.droite();
        }
	}
	
	@Override
	/**
	 * Permet de savoir si le zombie peut aller sur cette case
	 * suite a la direction qui veut prendre
	 * @return boolean
	 */
	protected boolean peutDeplacer(char vecteur, int x, int y) {
		boolean b = true;
		if(this.laby.getPerso().posX==x && this.laby.getPerso().posY==y) {
			b=false;
		}
		
		for(Entite m : this.laby.getMonstres()) {
			if(m.getPosX()==x && m.getPosY() == y) {
				b=false;
			}
		}
		
		if(laby.getCase(x, y).getType().equals("Mur")) {
			b=false;
		}
		return b;
	}

	@Override
	public void dessiner(Graphics2D crayon) {
		super.dessiner(crayon);
		crayon.setPaint(zombieSprite);
		crayon.fillOval(this.posX * TAILLE_CASE ,this.posY * TAILLE_CASE , TAILLE_CASE, TAILLE_CASE);
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Zombie";
	}
}
