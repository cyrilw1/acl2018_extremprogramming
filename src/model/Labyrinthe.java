package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.swing.text.AbstractDocument.LeafElement;

public class Labyrinthe {
    private Case[][] lab;
    private static String[] typeMonstres = {"Fantome", "Zombie", "Vampire"};
    private int length;
    private int width;

    /**
     * Heros du jeu
     */
    private Personnage perso;

    /**
     * Listes des monstres present sur la map
     */
    private ArrayList<Entite> monstres;

    private Astar astar;
    private ArrayList<Node> nodes;

    public Labyrinthe(int length, int width, int nbMonstres) {
        lab = new Case[length][width];
        this.length = length;
        this.width = width;
        genererLabyrinthe(75);
        buildGraph();

        this.perso = new Personnage(2, 2, this);
        this.monstres = new ArrayList<Entite>();
        this.genererMonstres(nbMonstres);
    }

    /**
     * Constructeur pour la classe de test qui initialise tout a vide
     * sauf la taille de la map
     *
     * @param length
     * @param width
     */
    public Labyrinthe(int length, int width) {
        lab = new Case[length][width];

        this.length = length;
        this.width = width;
        this.monstres = new ArrayList<Entite>();
    }

    /**
     * Methode qui g�n�re les monstres a des positions al�atoire
     *
     * @param nbMonstres, nombre de monstre � g�n�rer
     */
    public void genererMonstres(int nbMonstres) {
        for (int i = 0; i < nbMonstres; i++) { //genere monstre aleatoire et pos aleatoire
            Random r = new Random();
            int ale = r.nextInt(this.typeMonstres.length);
            System.out.println(ale);
            boolean b = false;
            while (!b) {
                int posx = (int) (Math.random() * (length - 4) + 2);
                int posy = (int) (Math.random() * (width - 4) + 2);
                if (lab[posx][posy].getType() == "Vide") {
                    switch (this.typeMonstres[ale]) {
                        case "Vampire":
                            this.monstres.add(new Vampire(posx, posy, this, astar, nodes, perso));
                            break;
                        case "Fantome":
                            this.monstres.add(new Fantome(posx, posy, this));
                            break;
                        case "Zombie":
                            this.monstres.add(new Zombie(posx, posy, this));
                            break;
                    }
                    b = true;
                }
            }
        }

        //temporaire le temps que plus de 1 vampire bug
		/*boolean b = false;
		while(!b) {
			int posx = (int) (Math.random() * (length - 4) + 2);
			int posy = (int) (Math.random() * (width - 4) + 2);
			if(lab[posx][posy].getType()=="Vide") {
				this.monstres.add(new Vampire(posx, posy, this, astar, nodes, perso));
				b=true;
			}
		}*/
    }

    /**
     * Creer un labyrinthe a partir d'un fichier texte
     * passe en parametre
     *
     * @param source, nom du fichier
     * @return le tableau de case correspondant au fichier texte
     */
    public Case[][] creerLabyFromTxt(String source) {
        BufferedReader helpReader;
        try {
            helpReader = new BufferedReader(new FileReader(source));
            String ligne;
            int y = 0;
            while ((ligne = helpReader.readLine()) != null) {
                for (int x = 0; x < ligne.length(); x++) {
                    switch (ligne.charAt(x)) {
                        case 'M':
                            ajouterCase(x, y, new CaseMur(x, y));
                            break;
                        case 'O':
                            ajouterCase(x, y, new CaseVide(x, y));
                            break;
                    }
                }
                y++;
            }
            helpReader.close();
        } catch (IOException e) {
            System.out.println("Nom de fichier nom valide");
        }
        return lab;
    }


    public Case[][] genererLabyrinthe(int NbObstacle) {

        //Le labyrinthe est vide
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < length; i++) {
                ajouterCase(i, j, new CaseVide(i, j));
            }
        }

        //On ajoute les murs
        for (int i = 0; i < length; i++) {
            ajouterCase(i, 0, new CaseMur(i, 0));
            ajouterCase(i, length - 1, new CaseMur(i, length - 1));
        }
        for (int j = 0; j < width; j++) {
            ajouterCase(0, j, new CaseMur(0, j));
            ajouterCase(width - 1, j, new CaseMur(width - 1, j));
        }

        //On ajoute quelques obstacles
        for (int i = 0; i < NbObstacle; i++) {
            //On récupère une position pour l'obstacle
            int posx = (int) (Math.random() * (length - 4) + 3);
            int posy = (int) (Math.random() * (width - 4) + 3);

            //Des gros obstacles
            ajouterCase(posx, posy, new CaseMur(posx, posy));
            ajouterCase(posx, posy + 1, new CaseMur(posx, posy + 1));
            ajouterCase(posx + 1, posy, new CaseMur(posx + 1, posy));
            ajouterCase(posx + 1, posy + 1, new CaseMur(posx + 1, posy + 1));
        }

        //on ajoute la case de teleportation
        boolean b = false;
        while (!b) {
            int posx = (int) (Math.random() * (length - 4) + 2);
            int posy = (int) (Math.random() * (width - 4) + 2);
            if (lab[posx][posy].getType() == "Vide") {
                ajouterCase(posx, posy, new CaseTeleportation(posx, posy));
                b = true;
            }
        }

        //Pour pimenter un peu le tout, on ne génére pas tout le temps le tresor. (une chance sur 3!)
        double tresor = Math.random();
        if(tresor < 0.33)
        	b = true;
        else
        	b = false;
        //Et maintenant on ajoute le coffre (ou pas hein)
        while (!b) {
            int posx = (int) (Math.random() * (length - 4) + 2);
            int posy = (int) (Math.random() * (width - 4) + 2);
            if (lab[posx][posy].getType() == "Vide") {
                ajouterCase(posx, posy, new CaseTresor(posx, posy));
                b = true;
            }
        }

        //On ajoute quelques cases piège
        for (int i = 0; i < 3; i++) {
            int posx = (int) (Math.random() * (length - 4) + 2);
            int posy = (int) (Math.random() * (width - 4) + 2);
            if (lab[posx][posy].getType() != "Vide") {
                posx = (int) (Math.random() * (length - 4) + 2);
                posy = (int) (Math.random() * (width - 4) + 2);
            }
            ajouterCase(posx, posy, new CasePiege(posx, posy));
        }
        
        //On ajoute quelques cases bonus
        for (int i = 0; i < 2; i++) {
            int posx = (int) (Math.random() * (length - 4) + 2);
            int posy = (int) (Math.random() * (width - 4) + 2);
            if (lab[posx][posy].getType() != "Vide") {
                posx = (int) (Math.random() * (length - 4) + 2);
                posy = (int) (Math.random() * (width - 4) + 2);
            }
            ajouterCase(posx, posy, new CaseBonus(posx, posy));
        }


        return lab;
    }

    /**
     * Permet d'ajouter une case dans le labyrinthe vide
     *
     * @param i, ligne de la case
     * @param j, colonne de la case
     * @param c, type de case a ajouter
     */
    public void ajouterCase(int i, int j, Case c) {
        //verif que la case soit bien dans le tableau
        if ((i >= 0 && i < lab.length) && (j >= 0 && j < lab[0].length)) {
            lab[i][j] = c;
        }
    }

    public void dessiner(Graphics2D crayon) {
        for (Case[] c : lab) {
            for (Case c2 : c) {
                c2.dessiner(crayon);
            }
        }
        this.perso.dessiner(crayon);
        for (Entite m : monstres) {
            m.dessiner(crayon);
        }
    }

    public Case[][] getLab() {
        return this.lab;
    }

    public Case getCase(int i, int j) {
        if (i >= 0 && i < length && j >= 0 && j < width) {
            return lab[i][j];
        } else
            return null;
    }


    /**
     * Methode qui print le labyrinthe sur forme de txt
     */
    public void dessinerLabyrintheConsole() {
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < width; j++) {
                if (lab[j][i].getType() == "Mur")
                    System.out.print("X");
                if (lab[j][i].getType() == "Vide")
                    System.out.print("O");
            }
            System.out.println("");
        }
        System.out.println("");
        System.out.println("");
        System.out.println("");
    }

    public int getLength() {
        return length;
    }

    public int getWidth() {
        return width;
    }

    public void buildGraph() {
        buildNodes();
        buildEdges();
        astar = new Astar();
    }

    private void buildEdges() {
        for (int y = 0; y < lab[0].length; y++) {
            for (int x = 0; x < lab.length; x++) {
                ArrayList<Edge> edgesA = new ArrayList<>();
                if (lab[x][y].getType().equals("Vide")) {
                    if (y + 1 < lab[0].length) {
                        if (lab[x][y + 1].getType().equals("Vide")) {
                            edgesA.add(new Edge(nodes.get(convertXYToInt(x, y + 1)), 1));
                        }
                    }
                    if (y - 1 >= 0) {
                        if (lab[x][y - 1].getType().equals("Vide")) {
                            edgesA.add(new Edge(nodes.get(convertXYToInt(x, y - 1)), 1));
                        }
                    }
                    if (x + 1 < lab.length) {
                        if (lab[x + 1][y].getType().equals("Vide")) {
                            edgesA.add(new Edge(nodes.get(convertXYToInt(x + 1, y)), 1));
                        }
                    }
                    if (x - 1 >= 0) {
                        if (lab[x - 1][y].getType().equals("Vide")) {
                            edgesA.add(new Edge(nodes.get(convertXYToInt(x - 1, y)), 1));
                        }
                    }
                }
                Edge[] edgesB = new Edge[edgesA.size()];
                edgesB = edgesA.toArray(edgesB);
                nodes.get(convertXYToInt(x, y)).addVoisin(edgesB);
            }
        }
    }

    private void buildNodes() {
        nodes = new ArrayList<>();
        for (int y = 0; y < lab[0].length; y++) {
            for (int x = 0; x < lab.length; x++) {
                nodes.add(new Node(convertXYToInt(x, y), x, y));
            }
        }
    }

    private int convertXYToInt(int x, int y) {
        return lab.length * y + x;
    }

    public Personnage getPerso() {
        return this.perso;
    }

    public ArrayList<Entite> getMonstres() {
        return this.monstres;
    }

    /**
     * Methode qui va mettre a jour la liste de monstres en
     * fonctions des monstres encore en vie
     */
    public void actualiserMonstres() {
        // TODO Auto-generated method stub
        ArrayList<Entite> l = new ArrayList<Entite>();
        for (Entite e : monstres) {
            if (!e.etreMort()) {
                l.add(e);
            }
        }
        monstres = l;
    }

    public void setPerso(Personnage p) {
        perso = p;
    }


}