package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Fantome extends Monstre {

	private static final int TAILLE_CASE = PacmanPainter.TAILLE_CASE;
	private BufferedImage ghostImg = null;
	private TexturePaint ghostSprite;

	public Fantome(int x, int y, Labyrinthe laby) {
		super(x, y, laby);
		pvMax = 3;
		pv = pvMax;
		degat = 1;
		ghostImg = Texture.getInstance().FANTOME;

		ghostSprite = new TexturePaint(ghostImg, new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
	}

	@Override
	/**
	 * Methode de deplacement du fantome, il peut traverser tout les murs sauf les
	 * murs limites de la map
	 */
	public void deplacer() {
		if (!mort) {
			double d = Math.random();
			char vecteur;
			if (d < 0.25) {
				vecteur = 'h';
				if (peutDeplacer(vecteur, 0, 0)) // monter
					this.haut();
			} else if (d < 0.5) {
				vecteur = 'b';
				if (peutDeplacer(vecteur, 0, 0)) // descendre
					this.bas();
			} else if (d < 0.75) {
				vecteur = 'g';
				if (peutDeplacer(vecteur, 0, 0)) // gauche
					this.gauche();
			} else {
				vecteur = 'd';
				if (peutDeplacer(vecteur, 0, 0)) // droite
					this.droite();
			}
		}
	}

	/**
	 * Methode qui donne si oui ou non on peut se deplacer dans cette direction
	 * 
	 * @param         x, position
	 * @param y
	 * @param vecteur
	 * @return
	 */
	@Override
	protected boolean peutDeplacer(char vecteur, int x, int y) {
		boolean peut = false;
		if (vecteur == 'h' && posY > 0) {
			peut = true;
		} else if (vecteur == 'b' && posY < (laby.getLength() - 1)) {
			peut = true;
		} else if (vecteur == 'g' && posX > 0) {
			peut = true;
		} else if (vecteur == 'd' && posX < (laby.getWidth() - 1)) {
			peut = true;
		}
		return peut && !mort;
	}

	@Override
	public void dessiner(Graphics2D crayon) {
		super.dessiner(crayon);
		crayon.setPaint(ghostSprite);
		crayon.fillRect(this.posX * TAILLE_CASE, this.posY * TAILLE_CASE, TAILLE_CASE, TAILLE_CASE);
	}

	@Override
	public String getType() {
		return "Fantome";
	}




}
