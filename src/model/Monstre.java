package model;

public abstract class Monstre extends Entite {

	public Monstre(int x, int y, Labyrinthe lab) {
		super(x, y, lab);
	}

	/**
	 * Methode qui permet au monstre d'attquer d�s que le heros est sur une case adjacente
	 * passe l'attribut @etatAttaque a vrai
	 */
	public void attaquer() {
		Personnage heros = laby.getPerso();
		if (this.posX == heros.getPosX() && this.posY - 1 == heros.getPosY()) {
			heros.subirDegat(this.degat);
			etatAttaque = true;
		}else if (this.posX == heros.getPosX() && this.posY + 1 == heros.getPosY()) {
			heros.subirDegat(this.degat);
			etatAttaque = true;
		}else if (this.posX - 1 == heros.getPosX() && this.posY == heros.getPosY()) {
			heros.subirDegat(this.degat);
			etatAttaque = true;
		}else if (this.posX + 1 == heros.getPosX() && this.posY == heros.getPosY()) {
			heros.subirDegat(this.degat);
			etatAttaque = true;
		}else {
			etatAttaque = false;
		}

	}

}
