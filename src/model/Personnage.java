package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import engine.Cmd;

public class Personnage extends Entite {

	private BufferedImage linkImg = null;
	private TexturePaint linkHaut, linkBas, linkGauche, linkDroite;
	private int TAILLE_CASE = PacmanPainter.TAILLE_CASE;

	public Personnage(int x, int y, Labyrinthe laby) {
		super(x, y, laby);
		this.pvMax = 200;
		this.pv = pvMax;
		this.degat = 1;
		linkImg = Texture.getInstance().HERO;

		linkBas = new TexturePaint(linkImg.getSubimage(28, 0, 25, 25), new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
		linkHaut = new TexturePaint(linkImg.getSubimage(57, 0, 25, 25), new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
		linkGauche = new TexturePaint(linkImg.getSubimage(146, 0, 25, 25),
				new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
		linkDroite = new TexturePaint(linkImg.getSubimage(180, 0, 25, 25),
				new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
	}

	@Override
	public void deplacer() {}

	@Override
	public boolean peutDeplacer(char c, int x, int y) {
		boolean b = true;
		for(Entite m : this.laby.getMonstres()) {
			if(m.getPosX()==x && m.getPosY() == y) {
				b=false;
			}
		}
		if(laby.getCase(x, y).getType().equals("Mur")) {
			b=false;
		}
		return b;
    }

	public void deplacer(Cmd commande) {
		switch (commande) {
		case UP:
			if (peutDeplacer('h', posX, posY - 1))
				this.haut();
			this.direction = Entite.HAUT;
			break;
		case DOWN:
			if (peutDeplacer('b', posX, posY + 1))
				this.bas();
			this.direction = Entite.BAS;
			break;
		case LEFT:
			if (peutDeplacer('g', posX - 1, posY))
				this.gauche();
			this.direction = Entite.GAUCHE;
			break;
		case RIGHT:
			if (peutDeplacer('d', posX + 1, posY))
				this.droite();
			this.direction = Entite.DROITE;
			break;
		default:
			this.pause();
			break;
		}
	}

	@Override
	public String getType() {
		return "Heros";
	}
	

	@Override
	public void attaquer() {
		for(Entite e : laby.getMonstres()) {
			switch (direction) {
			case Entite.HAUT:
				if(this.posX == e.getPosX() && this.posY - 1 == e.getPosY())
					e.subirDegat(this.degat);
				break;
			case Entite.BAS:
				if(this.posX == e.getPosX() && this.posY + 1 == e.getPosY())
					e.subirDegat(this.degat);
				break;
			case Entite.GAUCHE:
				if(this.posX - 1 == e.getPosX() && this.posY == e.getPosY())
					e.subirDegat(this.degat);
				break;
			case Entite.DROITE:
				if(this.posX + 1 == e.getPosX() && this.posY == e.getPosY())
					e.subirDegat(this.degat);
				break;
			}
		}
	}
	
	public void seSoigner() {
		this.pv = pvMax;
	}
	
	public void dessiner(Graphics2D crayon) {
		super.dessiner(crayon);
		// En fonction de la direction on utilise des sprites differents;
		switch (direction) {
		case Entite.GAUCHE:
			crayon.setPaint(linkGauche);
			break;
		case Entite.DROITE:
			crayon.setPaint(linkDroite);
			break;
		case Entite.HAUT:
			crayon.setPaint(linkHaut);
			break;
		case Entite.BAS:
			crayon.setPaint(linkBas);
			break;
		}

		crayon.fillRect(posX * TAILLE_CASE, posY * TAILLE_CASE, TAILLE_CASE, TAILLE_CASE);
	}


}

