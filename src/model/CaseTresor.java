package model;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CaseTresor extends Case {
	
    private TexturePaint tresorSprite;
    private int TAILLE_CASE = PacmanPainter.TAILLE_CASE;

	public CaseTresor(int a, int b) {
		super(a, b);  
		BufferedImage tresorImg = Texture.getInstance().MURTRESOR;
		tresorSprite = new TexturePaint(tresorImg, new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
	}

	@Override
	public void dessiner(Graphics2D crayon) {
		crayon.setPaint(tresorSprite);
        crayon.fillRect(x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE, TAILLE_CASE);
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Tresor";
	}

}
