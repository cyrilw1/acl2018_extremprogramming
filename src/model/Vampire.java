package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

public class Vampire extends Monstre {


    private Astar astar;
    private ArrayList<Node> nodes;
    private Personnage heros;
    private int joue;
    private List<Node> way;
    private int caseM = 0;


    private static final int TAILLE_CASE = PacmanPainter.TAILLE_CASE;
    private BufferedImage vampireImg = null;
    private TexturePaint vampireSprite;

    public Vampire(int x, int y, Labyrinthe lab, Astar a, ArrayList<Node> n, Personnage perso) {
        super(x, y, lab);
        pvMax = 7;
        pv = pvMax;
        degat = 3;
        joue = 10;
        astar = a;
        nodes = n;
        heros = perso;
        way = new ArrayList<>();

        try {
            vampireImg = ImageIO.read(new File("Img/vampire.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        vampireSprite = new TexturePaint(vampireImg, new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
    }

    private int convertXYToInt(int x, int y) {
        return laby.getLab().length * y + x;
    }

    private void deplacementIntelligent() {
        int tailleCase = PacmanPainter.TAILLE_CASE;
        int posPerso = convertXYToInt(heros.getPosX(), heros.getPosY());
        int posMonster = convertXYToInt(posX, posY);
        Node nP = nodes.get(posPerso);
        Node nM = nodes.get(posMonster);
        int distanceMP = Math.abs(nM.getX() - nP.getX()) + Math.abs(nM.getY() - nP.getY());//distance manhattan monster-perso

		/*  If Manhattan distance between monster node and character node < 20
			Then the monster use A* to find the character
			Else monster move are random */

        if (distanceMP < 20) {
            astar.AstarSearch(nM, nP);
            way = astar.printPath(nP);
            for (Node n : nodes) {
                n.setParent(null);
            }
        }
        if (way.size() > 1) caseM = way.remove(0).getNumber();
        else {
            double d = Math.random();
            char vecteur;
            if (d < 0.25) {
                vecteur = 'h';
                if (peutDeplacer(vecteur, posX, posY - 1)) //monter
                    this.haut();
            } else if (d < 0.5) {
                vecteur = 'b';
                if (peutDeplacer(vecteur, posX, posY + 1)) //descendre
                    this.bas();
            } else if (d < 0.75) {
                vecteur = 'g';
                if (peutDeplacer(vecteur, posX - 1, posY)) //gauche
                    this.gauche();
            } else {
                vecteur = 'd';
                if (peutDeplacer(vecteur, posX + 1, posY))   //droite
                    this.droite();
            }
        }
        //deplacements
        if (caseM + 1 == posMonster) gauche();
        else if (caseM + laby.getWidth() == posMonster) haut();
        else if (caseM - 1 == posMonster) droite();
        else if (caseM - laby.getLength() == posMonster) bas();
    }

    @Override
    public void deplacer() {
        deplacementIntelligent();
    }

    @Override
    protected boolean peutDeplacer(char c, int x, int y) {
        boolean b = true;
        if (this.laby.getPerso().posX == x && this.laby.getPerso().posY == y) {
            b = false;
        }

        for (Entite m : this.laby.getMonstres()) {
            if (m.getPosX() == x && m.getPosY() == y) {
                b = false;
            }
        }

        if (!laby.getCase(x, y).getType().equals("Vide")) {
            b = false;
        }
        return b;
    }

    @Override
    public void dessiner(Graphics2D crayon) {
        // TODO Auto-generated method stub
        super.dessiner(crayon);
        crayon.setPaint(vampireSprite);
        crayon.fillOval(this.posX * tailleCase, this.posY * tailleCase, tailleCase, tailleCase);
    }


    @Override
    public String getType() {
        return "Vampire";
    }

}
