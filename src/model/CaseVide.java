package model;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CaseVide extends Case {
    private  BufferedImage floorImg = null;
    private TexturePaint floorSprite;
    
    private int TAILLE_CASE = PacmanPainter.TAILLE_CASE;
    
    public CaseVide(int a, int b) {
        super(a, b);
        floorImg = Texture.getInstance().Herbe;
        
        floorSprite = new TexturePaint(floorImg, new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
    }


    public void dessiner(Graphics2D crayon) {
        crayon.setPaint(floorSprite);
        crayon.fillRect(x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE, TAILLE_CASE);
    }

    public String getType() {
        return "Vide";
    }

}
