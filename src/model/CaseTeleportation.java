package model;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CaseTeleportation extends Case{

	private BufferedImage wallImg = null;
    private TexturePaint wallSprite;
    private int TAILLE_CASE = PacmanPainter.TAILLE_CASE;
	
	public CaseTeleportation(int a, int b) {
		super(a,b);
//		try {
//            wallImg = ImageIO.read(new File(""));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        wallSprite = new TexturePaint(wallImg, new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
	}

	
	public void dessiner(Graphics2D crayon) {
		crayon.setColor(Color.black);
        //crayon.setPaint(wallSprite);
        crayon.fillRect(x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE, TAILLE_CASE);
		
	}

	public String getType() {
		return "Teleportation";
	}
}
