package model;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import engine.Cmd;
import engine.Game;

/**
 * @author la team XtremProgramming
 */
public class PacmanGame implements Game {


	private BufferedImage overImg = null;
	private BufferedImage winImg = null;
	private TexturePaint gameOver;
	private TexturePaint gameWin;

	/**
	 * La map du jeu
	 */
	private Labyrinthe lab;

	/**
	 * booleen qui donne l'état du jeu.
	 * false si le jeu n'est pas fini, true sinon
	 */
	private boolean fini;

	/**
	 * Nombre de monstre pr�sent sur la map
	 */
	private int nbMonstres;


	/**
	 * constructeur avec fichier source pour le help
	 */
	public PacmanGame(String source) {
		this.nbMonstres=7;
		this.lab = new Labyrinthe(40, 40,nbMonstres);
		this.fini = false;
		
		overImg = Texture.getInstance().GameOver;
		winImg = Texture.getInstance().GameWin;
		
		gameOver = new TexturePaint(overImg, new Rectangle(0, 0, PacmanPainter.DIM, PacmanPainter.DIM));
		gameWin = new TexturePaint(winImg, new Rectangle(0, 0, PacmanPainter.DIM, PacmanPainter.DIM));
	}




	/**
	 * faire evoluer le jeu suite a une commande
	 *
	 * @param commande
	 */
	@Override
	public void evolve(Cmd commande) {
		int persoX = lab.getPerso().posX;
		int persoY = lab.getPerso().posY;
		int tailleCase = PacmanPainter.TAILLE_CASE;

		Case[][] laby = lab.getLab();
		lab.getPerso().deplacer(commande);
		if(commande == Cmd.ATTAK) {
			lab.getPerso().attaquer();
			lab.actualiserMonstres();
		}
		
		
		//Cases qui ont un effet pour le jeu
		if(laby[persoX][persoY].getType().equals("Teleportation")) {
			nbMonstres++;
			this.lab = new Labyrinthe(40, 40,nbMonstres);
		}
		
		if(laby[persoX][persoY].getType().equals("Tresor")) {
			setFini(true);
		}
		
		//Pour le reste, on applique les effets de certaines cases
		laby[persoX][persoY].appliquerEffetJoueur(lab.getPerso());
		
		if(lab.getPerso().etreMort()) {
			this.fini=true;
		}

		evoluerMonstres();

	}

	/**
	 * Fait evoluer les monstres de la map
	 * si il peut attaquer il reste en mode attaque 
	 * sinon il se deplace
	 */
	private void evoluerMonstres() {
		for(Entite e: lab.getMonstres()) {
			e.attaquer();
			if(!e.etatAttaque) {
				e.deplacer();
			}
		}
	}

	/**
	 * verifier si le jeu est fini
	 */
	@Override
	public boolean isFinished() {
		return this.fini;
	}

	/**
	 * methode qui change l'état du jeu (fini ou non)
	 */
	public void setFini(boolean b) {
		this.fini = b;
	}

	public void dessiner(Graphics2D crayon) {
		if(!this.fini) {
			this.lab.dessiner(crayon);
		}else {
			if(this.lab.getPerso().etreMort()) {
				crayon.setPaint(gameOver);
				crayon.fillRect(0, 0, PacmanPainter.DIM, PacmanPainter.DIM);
			}else {
				crayon.setPaint(gameWin);
				crayon.fillRect(0, 0, PacmanPainter.DIM, PacmanPainter.DIM);
			}
		}
	}
}
