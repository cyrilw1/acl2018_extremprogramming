package model;
public class Edge{
    public final int cost;
    public final Node node;

    public Edge(Node nodeT, int costVal){
        node = nodeT;
        cost = costVal;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "cost=" + cost +
                ", node=" + node +
                '}';
    }
}
