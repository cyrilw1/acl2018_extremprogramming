package model;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class CaseMur extends Case {
    private BufferedImage wallImg = null;
    private TexturePaint wallSprite;
    private int TAILLE_CASE = PacmanPainter.TAILLE_CASE;
    
    public CaseMur(int a, int b) {
        super(a, b);
        wallImg = Texture.getInstance().MURINT;
        wallSprite = new TexturePaint(wallImg, new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
    }


    public void dessiner(Graphics2D crayon) {
        //crayon.setColor(Color.gray);
        crayon.setPaint(wallSprite);
        crayon.fillRect(x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE, TAILLE_CASE);
    }


    public String getType() {
        return "Mur";
    }

}
