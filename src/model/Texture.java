package model;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Texture {


	private static Texture texture;
	public static BufferedImage VAMPIRE ;
	public static BufferedImage FANTOME ;
	public static BufferedImage ZOMBIE ;
	public static BufferedImage HERO ;
	public static BufferedImage MUREXT ;
	public static BufferedImage MURINT ;
	public static BufferedImage MURBONUS ;
	public static BufferedImage MURPIEGE ;
	public static BufferedImage MURTRESOR ;
	public static BufferedImage Herbe ;
	public static BufferedImage GameOver;
	public static BufferedImage GameWin ;

	private Texture() {
		try {
			VAMPIRE = ImageIO.read(new File("Img/vampire.png"));
			FANTOME = ImageIO.read(new File("Img/ghost.png"));
			ZOMBIE = ImageIO.read(new File("Img/zombie.png"));
			HERO = ImageIO.read(new File("Img/link.png"));
			MURINT = ImageIO.read(new File("Img/wallSprite.png"));
			MUREXT = ImageIO.read(new File("Img/brickWall.png"));
			MURBONUS = ImageIO.read(new File("Img/bonus.png"));
			MURPIEGE = ImageIO.read(new File("Img/piege.png"));
			MURTRESOR = ImageIO.read(new File("Img/tresor.png"));
			Herbe = ImageIO.read(new File("Img/grass.png"));
			GameOver = ImageIO.read(new File("Img/Gameover.jpg"));
			GameWin = ImageIO.read(new File("Img/Win.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Texture getInstance() {
		if(texture==null) {
			texture = new Texture();
		}
		return texture;
	}


}
