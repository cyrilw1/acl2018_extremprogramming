package model;

import java.util.ArrayList;
import java.util.Arrays;

public class Node {

    private int x, y;
    private final int number;
    private int fCost; // cout final
    private int gCost; //cout mouvement
    private Node parent; // noeud parent pour retrouver le chemin le plus court
    private ArrayList<Edge> voisinageEdges; // voisin du noeud courant sans les murs

    public Node(int num, int x, int y) {
        number = num;
        this.x = x;
        this.y = y;
        voisinageEdges = new ArrayList<Edge>(4);
    }

    public int getX() {
        return x;
    }

    public ArrayList<Edge> getVoisinageEdges() {
        return voisinageEdges;
    }

    public int getY() {
        return y;
    }

    public int getfCost() {
        return fCost;
    }

    public int getNumber() {
        return number;
    }

    public int getgCost() {
        return gCost;
    }

    public Node getParent() {
        return parent;
    }

    public void addVoisin(Edge e) {
        voisinageEdges.add(e);
    }

    @Override
    public String toString() {
        return number + "";
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public void setgCost(int gCost) {
        this.gCost = gCost;
    }

    public void setfCost(int fCost) {
        this.fCost = fCost;
    }

    public void addVoisin(Edge[] edgesB) {
        voisinageEdges.addAll(Arrays.asList(edgesB));
    }
}
