package model;

import java.awt.Graphics2D;

public abstract class Case {
	protected int x,y;
	
	/**
	 * Creer une case avec une position
	 * @param a, position en x de la case
	 * @param b, position en y de la case
	 */
	public Case(int a, int b) {
		this.x = a;
		this.y = b;
	}
	
	public abstract void dessiner(Graphics2D crayon);
	public abstract String getType();

	public void appliquerEffetJoueur(Personnage perso) {
		// Vide par default!
		
	}
}
