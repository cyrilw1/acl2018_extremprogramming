package model;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CasePiege extends Case {
	
	private BufferedImage piegeImg = null;
    private TexturePaint piegeSprite;
    private int TAILLE_CASE = PacmanPainter.TAILLE_CASE;
    
	public CasePiege(int a, int b) {
        super(a, b);
        piegeImg = Texture.getInstance().MURPIEGE;
        piegeSprite = new TexturePaint(piegeImg, new Rectangle(0, 0, TAILLE_CASE, TAILLE_CASE));
	}

	@Override
	public void dessiner(Graphics2D crayon) {
		 //crayon.setColor(Color.gray);
        crayon.setPaint(piegeSprite);
        crayon.fillRect(x * TAILLE_CASE, y * TAILLE_CASE, TAILLE_CASE, TAILLE_CASE);

	}

	@Override
	public String getType() {
		return "Piege";
	}
	
	@Override
	public void appliquerEffetJoueur(Personnage p) {
		p.subirDegat(5);
	}

}
