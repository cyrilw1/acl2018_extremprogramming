package model;

import java.awt.Color;
import java.awt.Graphics2D;

public abstract class Entite {

	protected Labyrinthe laby;
	protected int posX;
	protected int posY;
	protected int direction;
	protected boolean enMouvement;
	final int tailleCase = PacmanPainter.TAILLE_CASE;

	protected int pvMax;
	protected int pv;
	protected int degat;
	protected boolean mort = false;

	/**
	 * Permet de savoir si le monstre peut attaquer le heros
	 */
	protected boolean etatAttaque = false;

	public static final int GAUCHE = 0, DROITE = 1, HAUT = 2, BAS = 3;

	public Entite(int x, int y, Labyrinthe lab) {
		this.posX = x;
		this.posY = y;
		enMouvement = false;
		laby = lab;
	}
	
	

	public void gauche() {
		direction = 0;
		enMouvement = true;
		deplacer(posX - 1, posY);
	}

	public void droite() {
		direction = 1;
		enMouvement = true;
		deplacer(posX + 1, posY);
	}

	public void haut() {
		direction = 2;
		enMouvement = true;
		deplacer(posX, posY - 1);
	}

	public void bas() {
		direction = 3;
		enMouvement = true;
		deplacer(posX, posY + 1);
	}

	public void pause() {
		enMouvement = false;
	}

	public void deplacer(int x, int y) {
		this.posX = x;
		this.posY = y;
	}

	/**
	 * Methode qui permet au entite d'attaquer une autre entite sur une case
	 * adjacente
	 */
	public abstract void attaquer();

	/**
	 * Methode de deplacement pour l'entite
	 */
	public abstract void deplacer();

	/**
	 * Methode qui permet de savoir si l'entite peut ou non se deplacer sur cette
	 * case dans la map
	 * 
	 * @param char c, direction que veut prendre l'entite
	 * @return boolean, vrai si on peut se deplacer faux si non
	 */
	protected abstract boolean peutDeplacer(char c, int x, int y);

	public abstract String getType();

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	public void dessiner(Graphics2D crayon) {
		if (pv >= 0) {
			int rouge = (int) ((1 - (double) pv / (double) pvMax) * 255.0);
			int vert = (int) (((double) pv / (double) pvMax) * 255.0);
			crayon.setColor(new Color(rouge, vert, 0));
		} else {
			crayon.setColor(Color.RED);
		}
		crayon.fillRect(posX * tailleCase, posY * tailleCase, (int) (tailleCase * (double) pv / (double) pvMax), 4);
	}

	/**
	 * Methode qui permet retire les points de vie d'une entite lorqu'elle est
	 * attaque
	 * 
	 * @param degat, nombre de pv a retirer
	 */
	public void subirDegat(int degat) {
		if (degat > 0) {
			if (!mort) {
				if ((pv - degat) > 0) {
					this.pv -= degat;
				} else {
					this.pv = 0;
					mort = true;
				}
			}
		}
	}

	public boolean etreMort() {
		return mort;
	}

	public boolean getEtatAttaque() {
		return etatAttaque;
	}

	public int getVie() {
		return this.pv;
	}
}
