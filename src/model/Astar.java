package model;

import java.util.*;

public class Astar {
    public void AstarSearch(Node start, Node end) {
        Comparator<Node> comparator = new Comparator<Node>() {
            @Override
            public int compare(Node node, Node t1) {
                return Integer.compare(node.getfCost(), t1.getfCost());
            }
        };

        //PriorityQueue sort node FCost in ascending order
        // openList contains nodes that need to be checked
        PriorityQueue<Node> openList = new PriorityQueue<Node>(comparator);
        //closedList contains nodes that have been checked
        ArrayList<Node> closedList = new ArrayList<Node>();

        start.setgCost(0); //Start node have GCost = 0
        openList.add(start);
        boolean isFound = false; //true if path is found

        while (!openList.isEmpty() && (!isFound)) {
            Node currentNode = openList.poll(); //get the node of openList which have the smaller FCost
            closedList.add(currentNode);
            if (currentNode.getNumber() == end.getNumber()) {
                isFound = true;
            }
            for (Edge e : currentNode.getVoisinageEdges()) { // For each current node neighbor
                Node voisin = e.node;
                int gCost = currentNode.getgCost() + e.cost; // movement cost
                // final cost = movement cost + Manhattan distance between each current node neighbor and end node
                int fCost = gCost + DManhattan(voisin, end);
                if ((!openList.contains(voisin) && !closedList.contains(voisin)) || gCost < voisin.getgCost()) {
                    voisin.setParent(currentNode);
                    voisin.setgCost(gCost);
                    voisin.setfCost(fCost);
                    if (closedList.contains(voisin)) {
                        closedList.remove(voisin);
                    }
                    if (!openList.contains(voisin)) {
                        openList.add(voisin);
                    }
                }
            }
            closedList.add(currentNode);
        }
    }

    private static int DManhattan(Node child, Node goal) {
        return Math.abs(goal.getX() - child.getX()) + Math.abs(goal.getY() - child.getY());
    }

    public List<Node> printPath(Node target) {
        List<Node> path = new ArrayList<Node>();
        for (Node node = target; node != null; node = node.getParent()) {
            path.add(node);
        }
        Collections.reverse(path);
        path.remove(0);
        return path;
    }
}
